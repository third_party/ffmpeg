#!/bin/bash -e

# Copyright (c) 2017 The Fuchsia Authors. All rights reserved.
# Use of this source code is governed by a BSD-style license that can be
# found in the LICENSE file.

# Use this to copy all config files into the tree.
for profile in default max; do
  # Copy config files for the architectures:
  #   - x64 has config.asm, config.h
  #   - arm64 has config.h
  for arch in x64 arm64; do
    # Don't waste time on non-existent configs, if no config.h then skip.
    [ ! -e "build.$arch/$profile/config.h" ] && continue
    for f in config.h config_components.h config.asm libavutil/avconfig.h libavutil/ffversion.h libavcodec/bsf_list.c libavcodec/codec_list.c libavcodec/parser_list.c libavformat/demuxer_list.c libavformat/muxer_list.c libavformat/protocol_list.c; do
      FROM="build.$arch/$profile/$f"
      TO="config/$profile/$arch/$f"
      if [ "$(dirname $f)" != "" ]; then mkdir -p $(dirname $TO); fi
      [ -e $FROM ] && cp -v $FROM $TO
    done
  done
done

echo "Copied all existing newer configs successfully."
